using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Driver : MonoBehaviour
{
    [SerializeField] float m_SteerSpeed = 50f;
    [SerializeField] float m_MoveSpeed = 30f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float steerAmount = Input.GetAxis("Horizontal") * m_SteerSpeed * Time.deltaTime;
        float moveAmount = Input.GetAxis("Vertical") * m_MoveSpeed * Time.deltaTime;

        transform.Rotate(0, 0, -steerAmount);
        transform.Translate(0f, moveAmount, 0f);
    }
}
